#include <stdio.h>
#include <dirent.h>
#include <time.h>
#include <unistd.h>

void fsplist(char dir[], char isadmin){
	if(	strstr(dir, "..") ||
		!strstr(dir, "FSP") || 
		(strstr(dir, "ADMIN") && !isadmin)){
		printf("NO PERM\n");
		return;
	}else{
		DIR* dirr = opendir(dir+1);
        	struct dirent* ent;
		if(dirr != NULL){
	        	while((ent = readdir(dirr)) != NULL){
	        	        printf("%s/%s\n", dir+1, ent->d_name);
		        }
		}
	}
}

void fspread(char dir[], char isadmin){
        if(     strstr(dir, "..") ||
                !strstr(dir, "FSP") || 
                (strstr(dir, "ADMIN") && !isadmin)){
                printf("NO PERM\n");
                return;
        }else{
		char buf[64];
                FILE* fl = fopen(dir+1, "r");
		FILE* f2 = fopen("/FSP/TMP", "w");
		if(fl != NULL){
			while(fgets(buf, sizeof(buf), fl) != NULL)
				fwrite(buf, sizeof(buf), 1, f2);
			fclose(fl);
		}
		else fwrite("FILE ERROR!\n", 16, 1, f2);
		fclose(f2);

		usleep(230000);
		fl = fopen("/FSP/TMP", "r");
		while(fgets(buf, sizeof(buf), fl) != NULL)
                                printf("%s", buf);
		fclose(fl);
		remove("/FSP/TMP");
	}
}

void fspwrite(char dir[], char isadmin){
	if(isadmin){
		char buf[64];
		FILE* fl = fopen(dir+1, "w");
		scanf("%64s", &buf);
		while(strcmp(buf, "__EOF")){
			fwrite(buf, sizeof(buf), 1, fl);
			scanf("%64s", &buf);
		}
		fclose(fl);
	}else{
		printf("NO PERM\n");
	}
}

void fspdel(char dir[], char isadmin){
        if(isadmin){
		remove(dir+1);
        }else{
                printf("NO PERM\n");
        }
}


void main(){
	char name[512], pass[512], adminpass[512], cmd[512], buff[512];
	char isadmin = 0;

	time_t sec = time(NULL);
	strftime(buff, sizeof(buff), "SYSTIME > %T", localtime(&sec));
	printf("%s\n", buff);
	scanf("%500s", &name);
	scanf("%500s", &pass);


	FILE* fl = fopen("/FSP/ADMIN/PASS", "r");
	fscanf(fl, "%500s", &adminpass);
	fclose(fl);

	if(!strcmp(name, "ADMIN") && !strcmp(pass,adminpass)) isadmin = 1;

	scanf("%500s", &cmd);

	switch(*cmd){
		case 'l':
			fsplist(cmd, isadmin);
			break;
		case 'r':
			fspread(cmd, isadmin);
			break;
		case 'w':
			fspwrite(cmd, isadmin);
			break;
		case 'd':
			fspdel(cmd, isadmin);
			break;
		case 'e':
			return;
	}
}
