FROM ubuntu

RUN apt-get update && apt-get install -y supervisor socat netcat; \
	mkdir /FSP/; \
	mkdir /filemgr/; \
	touch /FSP/TMP; \
	chmod 666 /FSP/TMP

ADD ./FSP/ /FSP/
ADD ./task /filemgr/task
ADD ./checker.sh /filemgr/checker.sh
ADD supervisord.conf /etc/supervisor/conf.d/supervisord.conf

WORKDIR /filemgr/

EXPOSE 4000:4000

ENV FLAG="mctf{3f8fae7ac04f390694a6d3f20a80a6ff}"
RUN echo $FLAG  > /FSP/ADMIN/FLAG
CMD ["/usr/bin/supervisord"]
